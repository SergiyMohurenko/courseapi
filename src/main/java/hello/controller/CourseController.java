package hello.controller;
        import hello.bean.Course;
        import hello.service.CourseService;
        import java.util.List;
        import org.springframework.web.bind.annotation.PathVariable;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("courses")
public class CourseController {
    CourseService courseService = new CourseService();
    @RequestMapping( method = RequestMethod.GET)
    public List<Course> getCourses() {
        List<Course> listOfCourses = courseService.getAllCourse2();
        return listOfCourses;}
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
        public Course getCourseById(@PathVariable("id") int id) {
        return courseService.getCourseById(id);
    }
    @RequestMapping(value = "/add", method = RequestMethod.POST)
        public Course addCourse(@RequestBody Course course) {
        return courseService.getaddCourse(course);}
    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
        public Course updateCourse(@RequestBody Course course) {
       return courseService.getUpdateCourse(course);}
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
        public void deleteCourse(@PathVariable("id") int id) {
        courseService.deleteCourse(id);}
}