package hello.service;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import hello.DB.Connections;
import hello.bean.Course;
import java.sql.*;
import java.sql.SQLException;

public class CourseService {
    static HashMap<Integer, Course> courseIdMap = getCourseIdMap();
    Connections db = null;
    static final String JDBC_DRIVER = "com.postgresql.jdbc.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost/course";
    static final String NAME = "courseName";
    static final String PRICE = "coursePrice";

    public CourseService() {
        super();

        if (courseIdMap == null) {
            courseIdMap = new HashMap<Integer, Course>();

            Course course1 = new Course(1, "Borshch", 1, "23", 12, false, false, true, false, false, false, false, false);
            Course course2 = new Course(2, "Soup", 1, "12", 12, true,false, false,true,false,false,false,false);
            courseIdMap.put(1, course1);
            courseIdMap.put(2, course2);

        }
        db = new Connections();
        System.out.println("DB opened successfully!");
    }
    public static HashMap<Integer, Course> getCourseIdMap() {

        return courseIdMap;
    }
    public List<Course> getAllCourse2() {
        List<Course> courses = new ArrayList<Course>(10);
        Statement stmt = null;
        int rows = 0;
        try {
            stmt = db.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from course inner join availability on course.id = availability.course_id order by id");

            while (rs.next()) {
                int id2 = rs.getInt("id");
                String courseName = rs.getString("title");
                int courseCategoryID = rs.getInt("course_type_id");
                String courseContent = rs.getString("description");
                int coursePrice = rs.getInt("price");
                boolean courseIsActive = rs.getBoolean("is_active");
                boolean courseAvMonday = rs.getBoolean("monday");
                boolean courseAvTuesday = rs.getBoolean("tuesday");
                boolean courseAvWednesday = rs.getBoolean("wednesday");
                boolean courseAvThursday = rs.getBoolean("thursday");
                boolean courseAvFriday = rs.getBoolean("friday");
                boolean courseAvSaturday = rs.getBoolean("saturday");
                boolean courseAvSunday = rs.getBoolean("sunday");
                rows++;
                Course course = new Course(id2, courseName, courseCategoryID, courseContent, coursePrice, courseIsActive
                       ,
                        courseAvMonday,
                        courseAvTuesday, courseAvWednesday, courseAvThursday, courseAvFriday, courseAvSaturday, courseAvSunday);
                courses.add(course);
            }

            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return courses;
    }

    public Course getCourseById(int id) {
        Statement stmt = null;
        Course course1 = new Course();
        try {
            stmt = db.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from course inner join availability on course.id = availability.course_id where id = " + id + ";");
            if (rs.next()) {
                int id2 = rs.getInt("id");
                String courseName = rs.getString("title");
                int courseCategoryID = rs.getInt("course_type_id");
                String courseContent = rs.getString("description");
                int coursePrice = rs.getInt("price");
                boolean courseIsActive = rs.getBoolean("is_active");
                boolean courseAvMonday = rs.getBoolean("monday");
                boolean courseAvTuesday = rs.getBoolean("tuesday");
                boolean courseAvWednesday = rs.getBoolean("wednesday");
                boolean courseAvThursday = rs.getBoolean("thursday");
                boolean courseAvFriday = rs.getBoolean("friday");
                boolean courseAvSaturday = rs.getBoolean("saturday");
                boolean courseAvSunday = rs.getBoolean("sunday");
                course1 = new Course(id, courseName, courseCategoryID, courseContent, coursePrice, courseIsActive, courseAvMonday,
                        courseAvTuesday, courseAvWednesday, courseAvThursday, courseAvFriday, courseAvSaturday, courseAvSunday);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return course1;
    }
    public Course getaddCourse(Course course)
    {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = 0;
        try
        {
            int index = 1;
            String sql1 = "insert into course (title, price, is_active, course_type_id, description) " +
                    "values(?,?,?,?,?) RETURNING id ";
            stmt = db.getConnection().prepareStatement(sql1);
            stmt.setString(index++,course.getCourseName());
            stmt.setInt(index++,course.getCoursePrice());
            stmt.setBoolean(index++,course.getCourseIsActive());
            stmt.setInt(index++,course.getCourseCategoryID());
            stmt.setString(index++,course.getCourseContent());
            rs = stmt.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            String sql3 = "insert into availability (course_id, monday, tuesday, wednesday, " +
                    "thursday, friday, saturday, sunday) " +
                    "values(?,?,?,?,?,?,?,?)RETURNING course_id";
            stmt = db.getConnection().prepareStatement(sql3);
            index = 1;
            stmt.setInt(index++,id);
            stmt.setBoolean(index++,course.getCourseAvMonday());
            stmt.setBoolean(index++,course.getCourseAvTuesday());
            stmt.setBoolean(index++,course.getCourseAvWednesday());
            stmt.setBoolean(index++,course.getCourseAvThursday());
            stmt.setBoolean(index++,course.getCourseAvFriday());
            stmt.setBoolean(index++,course.getCourseAvSaturday());
            stmt.setBoolean(index++,course.getCourseAvSunday());


            rs = stmt.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
                course.setId(id);
            }
        }
        catch ( Exception e )
        {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit( 0 );
        } finally {
            if (stmt != null) {
                try {
                    rs.close();
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println( "Insert!" );
        return course;
    }

    public Course getUpdateCourse(Course course)
    {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = 0;
        try
        {
            int index = 1;
            String sql1 = "update course set title = ?, price = ?, is_active = ?, course_type_id = ?, description = ? where id = " + course.getId() + " RETURNING id" ;
                   // "VALUES(?,?,?,?,?) RETURNING id ";
            stmt = db.getConnection().prepareStatement(sql1);
            stmt.setString(index++,course.getCourseName());
            stmt.setInt(index++,course.getCoursePrice());
            stmt.setBoolean(index++,course.getCourseIsActive());
            stmt.setInt(index++,course.getCourseCategoryID());
            stmt.setString(index++,course.getCourseContent());
            rs = stmt.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            String sql3 = "update availability set course_id = ?, monday = ?, tuesday = ?, wednesday = ?, " +
                    "thursday = ?, friday = ?, saturday = ?, sunday = ? where course_id = "+ id;
            stmt = db.getConnection().prepareStatement(sql3);
            index = 1;
            stmt.setInt(index++,id);
            stmt.setBoolean(index++,course.getCourseAvMonday());
            stmt.setBoolean(index++,course.getCourseAvTuesday());
            stmt.setBoolean(index++,course.getCourseAvWednesday());
            stmt.setBoolean(index++,course.getCourseAvThursday());
            stmt.setBoolean(index++,course.getCourseAvFriday());
            stmt.setBoolean(index++,course.getCourseAvSaturday());
            stmt.setBoolean(index++,course.getCourseAvSunday());
            stmt.executeUpdate();
            if (rs.next()) {
                id = rs.getInt(1);
                course.setId(id);
            }
        }
        catch ( Exception e )
        {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit( 0 );
        } finally {
            if (stmt != null) {
                try {
                    rs.close();
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println( "Update!" );
        return course;
    }
    public void deleteCourse(int id)
    {
        Statement stmt = null;
        try
        {
            stmt = db.getConnection().createStatement();

            String sql1 = "UPDATE course SET is_active = FALSE WHERE id = " + id;
            stmt.executeUpdate( sql1 );
            stmt.close();
        }
        catch ( Exception e )
        {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit( 0 );
        }

        System.out.println( "Delete successfully!" );
    }
}