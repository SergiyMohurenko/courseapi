package hello;
import hello.bean.Course;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
public class HelloController {

    @RequestMapping("/l")
    public String index() {
        return "Greetings from Spring Boot!";
    }

}
