
package hello.DB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class Connections {
    Connection connection = null;

    public Connections() {
        openConnection();
    }

    public void openConnection() {
        System.out.println("Connection init");
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("driver ok");
        } catch (ClassNotFoundException e) {

            System.out.println("PostgreSQL Driver was not found");
            e.printStackTrace();
            return;
        }
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://ec2-54-247-177-33.eu-west-1.compute.amazonaws.com:5432/d32r5e5f8dedec?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory", "hglbfqctknxrdd", "596515d8c0ee6119250969ade6355779ba44cdff29be4562a1b5723aef5cca44");
            System.out.println("jdbc ok");
        } catch (SQLException e) {

            System.out.println("No connection");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            System.out.println("Database OK");
        } else {
            System.out.println("No connection!");
        }
    }
    public Connection getConnection() {
        return connection;
    }
}

