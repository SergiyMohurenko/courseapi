package hello.bean;
public class Course {

    int id;
    String courseName;
    int courseCategoryID;
    String courseContent;
    int coursePrice;
    boolean courseIsActive;
    boolean courseAvMonday;
    boolean courseAvTuesday;
    boolean courseAvWednesday;
    boolean courseAvThursday;
    boolean courseAvFriday;
    boolean courseAvSaturday;
    boolean courseAvSunday;

    public Course() {
        super();
    }
    public Course(int i, String courseName, int courseCategoryID, String courseContent, int coursePrice, boolean courseIsActive,
                  boolean courseAvMonday, boolean courseAvTuesday, boolean courseAvWednesday, boolean courseAvThursday,
                  boolean courseAvFriday, boolean courseAvSaturday, boolean courseAvSunday
    ) {
        super();
        this.id = i;
        this.courseName = courseName;
        this.courseCategoryID = courseCategoryID;
        this.courseContent = courseContent;
        this.coursePrice = coursePrice;
        this.courseIsActive = courseIsActive;

        this.courseAvMonday = courseAvMonday;
        this.courseAvTuesday = courseAvTuesday;
        this.courseAvWednesday = courseAvWednesday;
        this.courseAvThursday = courseAvThursday;
        this.courseAvFriday = courseAvFriday;
        this.courseAvSaturday = courseAvSaturday;
        this.courseAvSunday  = courseAvSunday;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getCourseName() {
        return courseName;
    }
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
    public int getCourseCategoryID() {
        return courseCategoryID;
    }
    public void setCourseCategoryID(int courseCategoryID) {
        this.courseCategoryID = courseCategoryID;
    }
    public String getCourseContent() {
        return courseContent;
    }
    public void setCourseContent(String courseContent) {
        this.courseContent = courseContent;
    }
    public int getCoursePrice() {
        return coursePrice;
    }
    public void setCoursePrice(int coursePrice) {
        this.coursePrice = coursePrice;
    }
    public boolean getCourseIsActive() {
        return courseIsActive;
    }
    public void setCourseIsActive(boolean courseIsActive) {
        this.courseIsActive = courseIsActive;
    }
    public boolean getCourseAvMonday() {
        return courseAvMonday;
    }
    public void setCourseAvMonday(boolean courseAvMonday) {
        this.courseAvMonday = courseAvMonday;
    }
    public boolean getCourseAvTuesday() {
        return courseAvTuesday;
    }
    public void setCourseAvTuesday(boolean courseAvTuesday) {
        this.courseAvTuesday = courseAvTuesday;
    }
    public boolean getCourseAvWednesday() {
        return courseAvWednesday;
    }
    public void setCourseAvWednesday(boolean courseAvWednesday) {
        this.courseAvWednesday = courseAvWednesday;
    }
    public boolean getCourseAvThursday() {
        return courseAvThursday;
    }
    public void setCourseAvThursday(boolean courseAvThursday) {
        this.courseAvThursday = courseAvThursday;
    }
    public boolean getCourseAvFriday() {
        return courseAvFriday;
    }
    public void setCourseAvFriday(boolean courseAvFriday) {
        this.courseAvFriday = courseAvFriday;
    }
    public boolean getCourseAvSaturday() {
        return courseAvSaturday;
    }
    public void setCourseAvSaturday(boolean courseAvSaturday) {
        this.courseAvSaturday = courseAvSaturday;
    }
    public boolean getCourseAvSunday() {
        return courseAvSunday;
    }
    public void setCourseAvSunday(boolean courseAvSunday) {
        this.courseAvSunday = courseAvSunday;
    }
}